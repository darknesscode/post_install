#!/usr/bin/env bash
#  ____             _                         ____          _
# |  _ \  __ _ _ __| | ___ __   ___  ___ ___ / ___|___   __| | ___
# | | | |/ _' | '__| |/ / '_ \ / _ \/ __/ __| |   / _ \ / _' |/ _ \
# | |_| | (_| | |  |   <| | | |  __/\__ \__ \ |__| (_) | (_| |  __/
# |____/ \__,_|_|  |_|\_\_| |_|\___||___/___/\____\___/ \__,_|\___|
# -----------------------------------------------------------------
# https://darkncesscode.xyz
# https://github.com/codedarkness
# -----------------------------------------------------------------
#
#        FILE: linux-post-install.sh
#       USAGE: ./linux-post-install.sh
#
# DESCRIPTION: install all necesary to run a full system
#
#      AUTHOR: DarknessCode
#       EMAIL: achim@darknesscode.xyz
#
#     CREATED: 08-22-20 9:15
#
# -----------------------------------------------------------------

## Colors
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
BOLDGREEN="\e[1;${GREEN}m"
ITALICRED="\e[3;${RED}m"
ENDCOLOR="\e[0m"

os="$(cat /etc/os-release | grep PRETTY_NAME | cut -d = -f 2 | tr -d '"')"

arch() {
	config-files/archLinux.sh
}

debian() {
	config-files/debian.sh
}

fedora() {
	config-files/fedora.sh
}

void() {
	config-files/voidLinux.sh
}

suckless-apps() {
	config-files/suckLess.sh
}


press_enter() {
	echo ""
	echo -n "Press Enter To Continue"
	read
	clear
}

incorrect_selection() {
	echo "Incorrect selection! try again"
}

until [ "$selection" = "0" ]; do
	clear
	echo -e "${BLUE}DarknessCode${ENDCOLOR}"
	echo "  _____ _   _ _    _ _      _                   "
	echo " / ____| \ | | |  | | |    (_)                  "
	echo "| |  __|  \| | |  | | |     _ _ __  _   ___  __ "
	echo "| | |_ | . ' | |  | | |    | | '_ \| | | \ \/ / "
	echo "| |__| | |\  | |__| | |____| | | | | |_| |>  <  "
	echo " \_____|_| \_|\____/|______|_|_| |_|\__,_/_/\_\ "
	echo -e "${YELLOW}Post Script Installation${ENDCOLOR}"
	echo ""
	echo -e "You are in a [ ${RED}$os${ENDCOLOR} ] box"
	echo -e "${GREEN}[1]${ENDCOLOR} - Arch Linux"
	echo -e "${GREEN}[2]${ENDCOLOR} - Debian"
	echo -e "${GREEN}[3]${ENDCOLOR} - Fedora"
	echo -e "${GREEN}[4]${ENDCOLOR} - Void Linux"
	echo ""
	echo -e "${GREEN}[5]${ENDCOLOR} - Suckless"
	echo ""
	echo -e "${RED}*Note*${ENDCOLOR}"
	echo "Use this scripts in a fresh install of a system"
	echo "If running the scripts for testing, better use a vm"
	echo ""
	echo -e "${GREEN}[0]${ENDCOLOR} - Exit"
	echo ""
	echo -n "Enter selection [1 - 0] : "
	read selection
	echo ""

	case $selection in
		1) clear; arch   ;;
		2) clear; debian ;;
		3) clear; fedora ;;
		4) clear; void   ;;
		5) clear; suckless-apps ;;
		0) clear; exit ;;
		*) clear; incorrect_selection ; press_enter ;;
	esac
done
