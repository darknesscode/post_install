#!/usr/bin/env bash
#  ____             _                         ____          _
# |  _ \  __ _ _ __| | ___ __   ___  ___ ___ / ___|___   __| | ___
# | | | |/ _' | '__| |/ / '_ \ / _ \/ __/ __| |   / _ \ / _' |/ _ \
# | |_| | (_| | |  |   <| | | |  __/\__ \__ \ |__| (_) | (_| |  __/
# |____/ \__,_|_|  |_|\_\_| |_|\___||___/___/\____\___/ \__,_|\___|
# -----------------------------------------------------------------
# https://darkncesscode.xyz
# https://github.com/codedarkness
# -----------------------------------------------------------------
#
#        FILE: archLinux.sh
#       USAGE: ./archLinux.sh
#
# DESCRIPTION: install utilities and software in arch based systems
#
#      AUTHOR: DarknessCode
#       EMAIL: achim@darknesscode.xyz
#
#     CREATED: 07-08-20 10:49
#
# -----------------------------------------------------------------

## Colors
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
ENDCOLOR="\e[0m"

update_archlinux() {
	echo -e "${BLUE}Arch Linux${ENDCOLOR} Start to Update"
	echo ""
	sleep 2;

	sudo pacman -Syu --noconfirm --needed &&
	echo ""
	echo -e "${BLUE}Arch Linux${ENDCOLOR} Updated" || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
}

xorg-server() {
	echo -e "Installing ${YELLOW}Xorg Server${ENDCOLOR}"
	echo ""
	sleep 2

	PKGS=(xorg-server
	xorg-apps
	xorg-fonts-misc)

	sudo pacman -S --noconfirm --needed "${PKGS[@]}" &&
	echo ""
	echo -e "${YELLOW}Xorg Server${ENDCOLOR} successfully Installed" || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
}

utilities() {
	echo -e "Installing ${YELLOW}Utilities${ENDCOLOR} & Libraries"
	echo ""
	sleep 2

	echo -e "Cloning and installing yay"
	git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && cd .. &&
	sudo rm -r yay || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
	echo ""
	sleep 2

	PKGS=(wget
	curl
	scrot
	xautolock
	i3lock
	imagemagick
	alsa-utils
	lm_sensors
	networkmanager
	rsync
	arandr
	xarchiver
	zip
	unzip
	tlp
	polkit
	lxsession
	libx11
	libxft
	gcc
	make
	libxinerama
	gtk-engine-murrine
	dunst
	libnotify
	adapta-gtk-theme
	papirus-icon-theme
	ttf-dejavu
	ueberzug
   	noto-fonts)

	sudo pacman -S --noconfirm --needed "${PKGS[@]}" &&
	echo ""
	echo -e "${GREEN}Utilities & Libraries${ENDCOLOR} Successfully installed" || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
	echo ""

	PKGS=(xf86-video-amdgpu
	xf86-video-intel
	xf86-video-nouveau)

	PS3='Select A Video Draiver To Install : '
	select app in "${PKGS[@]}" "Quit"; do 
		if [ "$app" = "Quit" ]; then
			break
		elif [ ! "$app" ]; then
			echo -e "${RED}Invalid Option${ENDCOLOR}..."
		else
	        sudo pacman -S --noconfirm --needed $app && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		fi
		REPLY=
	done
}

tui-cli-apps() {
	echo -e "Installing ${GREEN}Tui & Cli${ENDCOLOR} Applications"
	echo ""
	sleep 2

	PKGS=(alacritty
	neovim
	neomutt
	amfora
	ranger
	bpytop
	htop
	neofetch
	mpv
	w3m)

	PS3='Select An App To Install : '
	select app in "${PKGS[@]}" "Quit"; do 
		if [ "$app" = "Quit" ]; then
			break
		elif [ ! "$app" ]; then
			echo -e "${RED}Invalid Option${ENDCOLOR}..."
		else
	        sudo pacman -S --noconfirm --needed $app && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		fi
		REPLY=
	done
}

gui-apps() {
	echo -e "Installing ${YELLOW}Gui${ENDCOLOR} Applications"
	echo ""
	sleep 2

	PKGS=(lxappearance
	xfce4-power-manager
	xfce4-appfinder
	pcmanfm
	sxiv
	zathura
	zathura-pdf-poppler
	gimp
	calibre
	libreoffice-fresh
	nitrogen
	chromium
	qutebrowser
	firefox)

	PS3='Select A GUI App To Install : '
	select app in "${PKGS[@]}" "brave" "Quit"; do 
		if [ "$app" = "Quit" ]; then
			break
		elif [ "$app" = "brave" ]; then
	        yay -S brave-bin && $app && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		elif [ ! "$app" ]; then
			echo -e "${RED}Invalid Option${ENDCOLOR}..."
		else
	        sudo pacman -S --noconfirm --needed $app && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		fi
		REPLY=
	done
}

suckless-apps() {
	config-files/suckLess.sh
}

directories-configs() {
	echo -e "${YELLOW}Directories & Config Files${ENDCOLOR}"
	echo ""
	sleep 2

	## xsessions directory
	### Check for dir, if not found create it using the mkdir ###
	xsessions="/usr/share/xsessions"
	[ ! -d "$xsessions" ] && sudo mkdir -p "$xsessions" &&
	echo "xsessions directory was created" || echo "$xsessions already exist!"
	echo ""

	sudo cp config-files/configs/dwm.desktop /usr/share/xsessions/dwm.desktop &&
	echo "dwm.desktop entry has been copied" || echo "I dunno what happend!!"
	echo ""

	## DWM directory
	### Check for dir, if not found create it using the mkdir ###
	dldir2="$HOME/.dwm"
	[ ! -d "$dldir2" ] && mkdir -p "$dldir2" &&
	echo "dwm directory was created" || echo "dwm directory already exist"
	echo ""

	cp -af config-files/configs/autostart.sh $HOME/.dwm/ &&
	echo "autostart has been copied" || echo "Holly Shhhhhh!!!"
	echo ""

	cp -af config-files/configs/sysact.sh $HOME/.dwm/ &&
	echo "system account files has been copied" || echo "We have a problem again!!!"
	echo ""

	cp -af config-files/configs/dmenu-programs.sh $HOME/.dwm/ &&
	echo "dmenu custom file has been copied" || echo "Not again!!!"
	echo ""

	cp -af config-files/configs/autostart_blocking.sh $HOME/.dwm/ &&
	echo "system autostart_blocking files has been copied" || echo "We have a problem again!!!"
	echo ""

	cp -ar config-files/configs/nmcli/ $HOME/.dwm/ &&
	echo "networkmanager demnu has been copied" || echo "We have a problem again!!!"
	echo ""

	cp -af config-files/configs/Xresources $HOME/.Xresources &&
	echo "New Xresources file has been copied" || echo "Againnnn!!!"
	echo ""

	cp -af config-files/configs/bash_profile $HOME/.bash_profile &&
	echo "New Bash Profile file has been copied" || echo "Againnnn!!!"
	echo ""

	cp -af config-files/configs/bashrc $HOME/.bashrc &&
	echo "New bashrc file has been copied" || echo "Againnnn!!!"
	echo ""

	sudo cp -af config-files/configs/blurlock /usr/bin/ &&
	sudo chmod +x /usr/bin/blurlock &&
	echo "blurlock has been installed" || echo "Sorry!!!"
	echo ""

	sudo cp -af config-files/configs/dc-scrot /usr/bin/ &&
	sudo chmod +x /usr/bin/dc-scrot &&
	echo "Scrot config files have been installed" || echo "Again!!!"
	echo ""

	echo "Cloning and installing Dracula gtk theme"
	git clone https://github.com/dracula/gtk.git &&
	sudo mv gtk /usr/share/themes/Dracula &&
	sudo chmod +x -R /usr/share/themes/Dracula &&
	echo "Dracula gtk theme has been installed" || echo "No way!!!!"
	echo ""

	## dunst directory
	### Check for dir, if not found create it using the mkdir ###
	dundir="$HOME/.config/dunst"
	[ ! -d "$dundir" ] && mkdir -p "$dundir" &&
	echo "dunst directory was created" || echo "$dundir already exist!"
	echo ""

	cp -af config-files/configs/dunstrc $HOME/.config/dunst/ &&
	echo "dunstrc file was copied" || echo "SSSSShhhh!!!!"
	echo ""

	## Desktop directory
	### Check for dir, if not found create it using the mkdir ###
	desktopdir="$HOME/Desktop"
	[ ! -d "$desktopdir" ] && mkdir -p "$desktopdir" &&
	echo "Desktop directory was created" || echo "$desktopdir already exist!"
	echo ""

	## Documents directory
	### Check for dir, if not found create it using the mkdir ###
	docdir="$HOME/Documents"
	[ ! -d "$docdir" ] && mkdir -p "$docdir" &&
	echo "Documents directory was created" || echo "$docdir already exist!"
	echo ""

	## Downloads directory
	### Check for dir, if not found create it using the mkdir ###
	dowdir="$HOME/Downloads"
	[ ! -d "$dowdir" ] && mkdir -p "$dowdir" &&
	echo "Downloads directory was created" || echo "$dowdir already exist!"
	echo ""

	## Music directory
	### Check for dir, if not found create it using the mkdir ###
	musdir="$HOME/Music"
	[ ! -d "$musdir" ] && mkdir -p "$musdir" &&
	echo "Music directory was created" || echo "$musdir already exist!"
	echo ""

	## Pictures directory
	### Check for dir, if not found create it using the mkdir ###
	picdir="$HOME/Pictures"
	[ ! -d "$picdir" ] && mkdir -p "$picdir" &&
	echo "Pictures directory was created" || echo "$picdir already exist!"
	echo ""

	## Videos directory
	### Check for dir, if not found create it using the mkdir ###
	vidir="$HOME/Videos"
	[ ! -d "$vidir" ] && mkdir -p "$vidir" &&
	echo "Videos directory was created" || echo "$vidir already exist!"
	echo ""

	## Templates directory
	### Check for dir, if not found create it using the mkdir ###
	temdir="$HOME/Templates"
	[ ! -d "$temdir" ] && mkdir -p "$temdir" &&
	echo "Videos directory was created" || echo "$temdir already exist!"
	echo ""
}

display-manager() {
	echo -e "${YELLOW}Lightdm & Mini-Greeter${ENDCOLOR} Display Manager"
	echo ""
	sleep 2

	sudo pacman -S --noconfirm --needed lightdm &&
	yay -S lightdm-mini-greeter &&
	sudo cp -af config-files/configs/lightdm-mini-greeter.conf /etc/lightdm/ &&
	echo "New mini greeter has been copied" || echo "Upss!!!!"
	echo ""
	sleep 2

	read -p " Which is your user name : " choice;
	sudo sed -i 's/user = CHANGE_ME/user = '$choice'/g' /etc/lightdm/lightdm-mini-greeter.conf &&
	echo "User added to lightdm-mini-greeter" || echo "No way!!!!"
	echo ""

	sudo sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=lightdm-mini-greeter/g' /etc/lightdm/lightdm.conf &&
	sudo sed -i 's/#user-session=default/user-session=dwm/g' /etc/lightdm/lightdm.conf &&

	echo "Lightdm setup is done!!" || echo "Something is happening!!!"

	sudo systemctl enable lightdm -f
	echo "Lightdm was enabled"
	echo ""

	while true; do
		read -p "Would you like to restart you system now [y - n] : " yn
		case $yn in
			[Yy]* )
				sudo reboot ;;
			[Nn]* )
				break ;;
			* ) echo "Please answer yes or no." ;;
		esac
	done
}

press_enter() {
	echo ""
	echo -n "Press Enter To Continue"
	read
	clear
}

incorrect_selection() {
	echo "Incorrect selection! try again"
}

until [ "$selection" = "0" ]; do
	clear
	echo -e "${BLUE}DarknessCode${ENDCOLOR}"
	echo "                   _     _      _                   "
	echo "    /\            | |   | |    (_)                  "
	echo "   /  \   _ __ ___| |__ | |     _ _ __  _   ___  __ "
	echo "  / /\ \ | '__/ __| '_ \| |    | | '_ \| | | \ \/ / "
	echo " / ____ \| | | (__| | | | |____| | | | | |_| |>  <  "
	echo "/_/    \_\_|  \___|_| |_|______|_|_| |_|\__,_/_/\_\ "
	echo -e "${YELLOW}Build a minimal linux system${ENDCOLOR}"
	echo -e "${GREEN}[1]${ENDCOLOR} - Update Arch Linux"
	echo -e "${GREEN}[2]${ENDCOLOR} - XORG Display Server"
	echo -e "${GREEN}[3]${ENDCOLOR} - Utilities"
	echo -e "${GREEN}[4]${ENDCOLOR} - TUI & CLI Applications"
	echo -e "${GREEN}[5]${ENDCOLOR} - GUI Applications"
	echo ""
	echo -e "${YELLOW}Wimdow Manager & Other Stuff${ENDCOLOR}"
	echo -e "${GREEN}[6]${ENDCOLOR} - Suckless [dwm, slstatus, st, dmenu]"
	echo ""
	echo -e "${GREEN}[7]${ENDCOLOR} - Directories & Config-Files"
	echo -e "${GREEN}[8]${ENDCOLOR} - Display Manager [Lightdm & Lightdm-mini-greeter]"
	echo ""
	echo -e "${GREEN}[0]${ENDCOLOR} - Main Menu"
	echo ""
	echo -n "Enter selection [1 - 0] : "
	read selection
	echo ""

	case $selection in
		1) clear; update_archlinux ; press_enter ;;
		2) clear; xorg-server ; press_enter ;;
		3) clear; utilities; press_enter;;
		4) clear; tui-cli-apps; press_enter;;
		5) clear; gui-apps; press_enter;;
		6) clear; suckless-apps;;
		7) clear; directories-configs; press_enter;;
		8) clear; display-manager; press_enter;;
		0) clear; exit ;;
		*) clear; incorrect_selection ; press_enter ;;
	esac
done
