#!/usr/bin/env bash
#  ____             _                         ____          _
# |  _ \  __ _ _ __| | ___ __   ___  ___ ___ / ___|___   __| | ___
# | | | |/ _' | '__| |/ / '_ \ / _ \/ __/ __| |   / _ \ / _' |/ _ \
# | |_| | (_| | |  |   <| | | |  __/\__ \__ \ |__| (_) | (_| |  __/
# |____/ \__,_|_|  |_|\_\_| |_|\___||___/___/\____\___/ \__,_|\___|
# -----------------------------------------------------------------
# https://darkncesscode.xyz
# https://github.com/codedarkness
# -----------------------------------------------------------------
#
#        FILE: suckLess.sh
#       USAGE: ./suckLess.sh
#
# DESCRIPTION: Install Suckless Applications from source
#
#      AUTHOR: DarknessCode
#       EMAIL: achim@darknesscode.xyz
#
#     CREATED: 02-10-23 15:05
#
# -----------------------------------------------------------------

## Colors
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
ENDCOLOR="\e[0m"

dwm() {
	echo -e "${YELLOW}dwm a dynamic window manager for X${ENDCOLOR}"
	sleep 2
	echo ""

	cd config-files/systems/dwm/ &&
	sudo make clean install &&
	echo -e "dwm ${GREE}installed${ENDCOLOR}" &&
	cd ../../.. || echo -e "${RED}LinuxSucks${ENDCOLOR}..."
}

dmenu() {
	echo -e "${YELLOW}dmenu is a dynamic menu for X${ENDCOLOR}"
	sleep 2
	echo ""

	cd config-files/systems/dmenu &&
	sudo make clean install &&
	echo -e "dmenu ${GREE}installed${ENDCOLOR}" &&
	cd ../../.. || echo -e "${RED}LinuxSucks${ENDCOLOR}..."
}

slstatus() {
	echo -e "${YELLOW}slstatus is a status monitor for window managers${ENDCOLOR}"
	sleep 2
	echo ""

	cd config-files/systems/slstatus &&
	sudo make clean install &&
	echo -e "slstatus ${GREE}installed${ENDCOLOR}" &&
	cd ../../.. || echo -e "${RED}LinuxSucks${ENDCOLOR}..."
}

st() {
	echo -e "${YELLOW}st is a simple terminal implementation for X${ENDCOLOR}"
	sleep 2
	echo ""
	echo " ### This is the built from Luke Smith ###"

	cd config-files/systems/st &&
	sudo make clean install &&
	echo -e "st simple terminal ${GREE}installed${ENDCOLOR}" &&
	cd ../../.. || echo -e "${RED}LinuxSucks${ENDCOLOR}..."
}

edit-dwm() {
	echo -e "Edit config.h for dwm"
	sleep 2

	vim config-files/systems/dwm/config.h
}

edit-slstatus() {
	echo -e "Edit config.h for slstatus"
	sleep 2

	vim config-files/systems/slstatus/config.h
}

edit-dmenu() {
	echo -e "Edit config.h for dmenu"
	sleep 2

	vim config-files/systems/dmenu/config.h
}

edit-st() {
	echo -e "Edit config.h for st simple terminal"
	sleep 2

	vim config-files/systems/st/config.h
}

press_enter() {
	echo ""
	echo -n "Press Enter To Continue"
	read
	clear
}

incorrect_selection() {
	echo "Incorrect selection! try again"
}

until [ "$selection" = "0" ]; do
	clear
	echo -e "${BLUE}DarknessCode${ENDCOLOR}"
	echo "  _____            _    _                    "
	echo " / ____|          | |  | |                   "
	echo "| (___  _   _  ___| | _| |     ___  ___ ___  "
	echo " \___ \| | | |/ __| |/ / |    / _ \/ __/ __| "
	echo " ____) | |_| | (__|   <| |___|  __/\__ \__ \ "
	echo "|_____/ \__,_|\___|_|\_\______\___||___/___/ "
	echo -e "${YELLOW}Software That Suck Less${ENDCOLOR}"
	echo -e "${YELLOW}Dynamic window manager & status bar${ENDCOLOR}"
	echo -e "${GREEN}[1]${ENDCOLOR} - dwm"
	echo -e "${GREEN}[2]${ENDCOLOR} - slstatus"
	echo ""
	echo -e "${YELLOW}Dynamic menu & tools${ENDCOLOR}"
	echo -e "${GREEN}[3]${ENDCOLOR} - dmenu"
	echo -e "${GREEN}[4]${ENDCOLOR} - st"
	echo ""
	echo -e "${RED}*Note*${ENDCOLOR}"
	echo "This is for a first install, If you have it install"
	echo "edit the config files and make your changes then re-install"
	echo ""
	echo -e "${GREEN}[1E]${ENDCOLOR} - edit [dwm]"
	echo -e "${GREEN}[2E]${ENDCOLOR} - edit [slstatus]"
	echo -e "${GREEN}[3E]${ENDCOLOR} - edit [dmnu]"
	echo -e "${GREEN}[4E]${ENDCOLOR} - edit [st]"
	echo ""
	echo -e "${GREEN}[0]${ENDCOLOR} - Exit"
	echo ""
	echo -n "Enter selection [1 - 0] : "
	read selection
	echo ""

	case $selection in
		1) clear; dwm ; press_enter ;;
		1E) clear; edit-dwm ; press_enter ;;
		2) clear; slstatus; press_enter ;;
		2E) clear; edit-slstatus ; press_enter ;;
		3) clear; dmenu ; press_enter ;;
		3E) clear; edit-dmenu ; press_enter ;;
		4) clear; st ; press_enter ;;
		4E) clear; edit-st ; press_enter ;;
		0) clear; exit ;;
		*) clear; incorrect_selection ; press_enter ;;
	esac
done
