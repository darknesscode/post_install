#!/usr/bin/env bash
#  ____             _                         ____          _
# |  _ \  __ _ _ __| | ___ __   ___  ___ ___ / ___|___   __| | ___
# | | | |/ _' | '__| |/ / '_ \ / _ \/ __/ __| |   / _ \ / _' |/ _ \
# | |_| | (_| | |  |   <| | | |  __/\__ \__ \ |__| (_) | (_| |  __/
# |____/ \__,_|_|  |_|\_\_| |_|\___||___/___/\____\___/ \__,_|\___|
# -----------------------------------------------------------------
# https://darkncesscode.xyz
# https://github.com/codedarkness
# -----------------------------------------------------------------
#
#        FILE: fedora.sh
#       USAGE: ./fedora.sh
#
# DESCRIPTION: install utilities and software in fedora based systems
#
#      AUTHOR: DarknessCode
#       EMAIL: achim@darknesscode.xyz
#
#     CREATED: 09-11-22 12:55
#
# -----------------------------------------------------------------

## Colors
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
ENDCOLOR="\e[0m"

update_fedora() {
	echo -e "${GREEN}Fedora${ENDCOLOR} Start to Update"
	echo ""
	sleep 2;

	sudo dnf check-update && sudo dnf upgrade &&
	echo ""
	echo -e "${GREEN}Fedora${ENDCOLOR} Updated" || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
}

xorg-server() {
	echo -e "Installing ${YELLOW}Xorg Server${ENDCOLOR}"
	echo ""
	sleep 2

	PKGS=(xorg-x11-server-Xorg
	xorg-x11-server-common
	xorg-x11-fonts-misc)

	sudo dnf install -y "${PKGS[@]}" &&
	echo ""
	echo -e "${YELLOW}Xorg Server${ENDCOLOR} successfully Installed" || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
}

utilities() {
	echo -e "Installing ${YELLOW}Utilities${ENDCOLOR} & Libraries"
	echo ""
	sleep 2

	PKGS=(wget
	curl
	scrot
	xautolock
	i3lock
	ImageMagick
	alsa-utils
	lm_sensors
	NetworkManager
	rsync
	arandr
	xarchiver
	zip
	unzip
	tlp
	polkit
	lxsession
	libX11-devel
	libXft-devel
	gcc
	make
	libXinerama-devel
	gtk-murrine-engine
	dunst
	libnotify
	papirus-icon-theme)

	sudo dnf install -y "${PKGS[@]}" &&
	sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm &&
	sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm &&
	echo ""
	echo -e "${GREEN}Utilities & Libraries${ENDCOLOR} Successfully installed" || echo -e "${RED}LinuxSucks......${ENDCOLOR}"
}

tui-cli-apps() {
	echo -e "Installing ${GREEN}Tui & Cli${ENDCOLOR} Applications"
	echo ""
	sleep 2

	PKGS=(rxvt-unicode
	alacritty
	neovim
	ranger
	btop
	htop
	neofetch
	mpv
	w3m)

	PS3='Select An App To Install : '
	select app in "${PKGS[@]}" "Quit"; do 
		if [ "$app" = "Quit" ]; then
			break
		elif [ ! "$app" ]; then
			echo -e "${RED}Invalid Option${ENDCOLOR}..."
		else
	        sudo dnf install -y $app && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		fi
		REPLY=
	done
}

gui-apps() {
	echo -e "Installing ${YELLOW}Gui${ENDCOLOR} Applications"
	echo ""
	sleep 2

	PKGS=(lxappearance
	xfce4-power-manager
	xfce4-appfinder
	pcmanfm
	sxiv
	zathura
	zathura-pdf-poppler
	gimp
	calibre
	libreoffice
	qutebrowser
	firefox
	nitrogen)

	PS3='Select A GUI App To Install : '
	select app in "${PKGS[@]}" "Quit"; do 
		if [ "$app" = "Quit" ]; then
			break
		elif [ "$app" = "brave" ]; then
			sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/ &&
			sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc &&
			sudo dnf install -y brave-browser && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		elif [ ! "$app" ]; then
			echo -e "${RED}Invalid Option${ENDCOLOR}..."
		else
	        sudo dnf install -y $app && echo "${GREEN}$app installed${ENDCOLOR}" || echo -e "${RED}LinuxSucks...${ENDCOLOR}"
		fi
		REPLY=
	done
}

suckless-apps() {
	config-files/suckLess.sh
}

directories-configs() {
	echo -e "${YELLOW}Directories & Config Files${ENDCOLOR}"
	echo ""
	sleep 2

	## xsessions directory
	### Check for dir, if not found create it using the mkdir ###
	xsessions="/usr/share/xsessions"
	[ ! -d "$xsessions" ] && sudo mkdir -p "$xsessions" &&
	echo "xsessions directory was created" || echo "$xsessions already exist!"
	echo ""

	sudo cp config-files/configs/dwm.desktop /usr/share/xsessions/dwm.desktop &&
	echo "dwm.desktop entry has been copied" || echo "I dunno what happend!!"
	echo ""

	## DWM directory
	### Check for dir, if not found create it using the mkdir ###
	dldir2="$HOME/.dwm"
	[ ! -d "$dldir2" ] && mkdir -p "$dldir2" &&
	echo "dwm directory was created" || echo "dwm directory already exist"
	echo ""

	cp -af config-files/configs/autostart.sh $HOME/.dwm/ &&
	echo "autostart has been copied" || echo "Holly Shhhhhh!!!"
	echo ""

	cp -af config-files/configs/sysact.sh $HOME/.dwm/ &&
	echo "system account files has been copied" || echo "We have a problem again!!!"
	echo ""

	cp -af config-files/configs/dmenu-programs.sh $HOME/.dwm/ &&
	echo "dmenu custom file has been copied" || echo "Not again!!!"
	echo ""

	cp -af config-files/configs/autostart_blocking.sh $HOME/.dwm/ &&
	echo "system autostart_blocking files has been copied" || echo "We have a problem again!!!"
	echo ""

	cp -ar config-files/configs/nmcli/ $HOME/.dwm/ &&
	echo "networkmanager demnu has been copied" || echo "We have a problem again!!!"
	echo ""

	cp -af config-files/configs/Xresources $HOME/.Xresources &&
	echo "New Xresources file has been copied" || echo "Againnnn!!!"
	echo ""

	cp -af config-files/configs/bash_profile $HOME/.bash_profile &&
	echo "New Bash Profile file has been copied" || echo "Againnnn!!!"
	echo ""

	cp -af config-files/configs/bashrc $HOME/.bashrc &&
	echo "New bashrc file has been copied" || echo "Againnnn!!!"
	echo ""
	sleep 2

	sudo cp -af config-files/configs/blurlock /usr/bin/ &&
	sudo chmod +x /usr/bin/blurlock &&
	echo "blurlock has been installed" || echo "Sorry!!!"
	echo ""

	sudo cp -af config-files/configs/dc-scrot /usr/bin/ &&
	sudo chmod +x /usr/bin/dc-scrot &&
	echo "Scrot config files have been installed" || echo "Again!!!"
	echo ""

	echo "Cloning and installing Dracula gtk theme"
	git clone https://github.com/dracula/gtk.git &&
	sudo mv gtk /usr/share/themes/Dracula &&
	sudo chmod +x -R /usr/share/themes/Dracula &&
	echo "Dracula gtk theme has been installed" || echo "No way!!!!"
	echo ""

	## dunst directory
	### Check for dir, if not found create it using the mkdir ###
	dundir="$HOME/.config/dunst"
	[ ! -d "$dundir" ] && mkdir -p "$dundir" &&
	echo "dunst directory was created" || echo "$dundir already exist!"
	echo ""

	cp -af config-files/configs/dunstrc $HOME/.config/dunst/ &&
	echo "dunstrc file was copied" || echo "SSSSShhhh!!!!"
	echo ""

	## Desktop directory
	### Check for dir, if not found create it using the mkdir ###
	desktopdir="$HOME/Desktop"
	[ ! -d "$desktopdir" ] && mkdir -p "$desktopdir" &&
	echo "Desktop directory was created" || echo "$desktopdir already exist!"
	echo ""

	## Documents directory
	### Check for dir, if not found create it using the mkdir ###
	docdir="$HOME/Documents"
	[ ! -d "$docdir" ] && mkdir -p "$docdir" &&
	echo "Documents directory was created" || echo "$docdir already exist!"
	echo ""

	## Downloads directory
	### Check for dir, if not found create it using the mkdir ###
	dowdir="$HOME/Downloads"
	[ ! -d "$dowdir" ] && mkdir -p "$dowdir" &&
	echo "Downloads directory was created" || echo "$dowdir already exist!"
	echo ""

	## Music directory
	### Check for dir, if not found create it using the mkdir ###
	musdir="$HOME/Music"
	[ ! -d "$musdir" ] && mkdir -p "$musdir" &&
	echo "Music directory was created" || echo "$musdir already exist!"
	echo ""

	## Pictures directory
	### Check for dir, if not found create it using the mkdir ###
	picdir="$HOME/Pictures"
	[ ! -d "$picdir" ] && mkdir -p "$picdir" &&
	echo "Pictures directory was created" || echo "$picdir already exist!"
	echo ""

	## Videos directory
	### Check for dir, if not found create it using the mkdir ###
	vidir="$HOME/Videos"
	[ ! -d "$vidir" ] && mkdir -p "$vidir" &&
	echo "Videos directory was created" || echo "$vidir already exist!"
	echo ""

	## Templates directory
	### Check for dir, if not found create it using the mkdir ###
	temdir="$HOME/Templates"
	[ ! -d "$temdir" ] && mkdir -p "$temdir" &&
	echo "Templates directory was created" || echo "$temdir already exist!"
}

display-manager() {
	echo -e "${YELLOW}Lightdm & Slick-Greeter${ENDCOLOR} Display Manager"
	echo ""
	sleep 2

	PKGS=(lightdm
	slick-greeter)

	sudo dnf install -y "${PKGS[@]}" &&
	echo "lightdm and slick-greeter successfully installed" || echo "LinuxSucks..."
	echo ""

	sudo cp -af config-files/configs/slick-greeter.conf /etc/lightdm/ &&
	echo "New slick greeter has been copied" || echo "Upss!!!!"
	echo ""

	sudo sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=slick-greeter/g' /etc/lightdm/lightdm.conf &&
	sudo sed -i 's/#user-session=default/user-session=dwm/g' /etc/lightdm/lightdm.conf &&

	echo "Lightdm setup is done!!" || echo "Something is happening!!!"
	echo ""

	sudo systemctl enable lightdm -f &&
	sudo systemctl set-default graphical.target
	echo "Lightdm was enabled"
	echo ""
	sleep 2

	while true; do
		read -p " Would you like to restart you system now [y - n] : " yn
		case $yn in
			[Yy]* )
				sudo reboot ;;
			[Nn]* )
				break ;;
			* ) echo "Please answer yes or no." ;;
		esac
	done
}

press_enter() {
	echo ""
	echo -n "Press Enter To Continue"
	read
	clear
}

incorrect_selection() {
	echo "Incorrect selection! try again"
}

until [ "$selection" = "0" ]; do
	clear
	echo -e "${BLUE}DarknessCode${ENDCOLOR}"
	echo " ______       _                  "
	echo "|  ____|     | |                 "
	echo "| |__ ___  __| | ___  _ __ __ _  "
	echo "|  __/ _ \/ _' |/ _ \| '__/ _' | "
	echo "| | |  __/ (_| | (_) | | | (_| | "
	echo "|_|  \___|\__,_|\___/|_|  \__,_| "
	echo -e "${YELLOW}Build a minimal linux system${ENDCOLOR}"
	echo -e "${GREEN}[1]${ENDCOLOR} - Update Fedora"
	echo -e "${GREEN}[2]${ENDCOLOR} - XORG Display Server"
	echo -e "${GREEN}[3]${ENDCOLOR} - Utilities"
	echo -e "${GREEN}[4]${ENDCOLOR} - TUI & CLI Applications"
	echo -e "${GREEN}[5]${ENDCOLOR} - GUI Applications"
	echo ""
	echo -e "${YELLOW}Wimdow Manager & Other Stuff${ENDCOLOR}"
	echo -e "${GREEN}[6]${ENDCOLOR} - Suckless [dwm, slstatus, st, dmenu]"
	echo ""
	echo -e "${GREEN}[7]${ENDCOLOR} - Directories & Config-Files"
	echo -e "${GREEN}[8]${ENDCOLOR} - Display Manager [Lightdm & Lightdm-mini-greeter]"
	echo ""
	echo -e "${GREEN}[0]${ENDCOLOR} - Main Menu"
	echo ""
	echo -n "Enter selection [1 - 0] : "
	read selection
	echo ""

	case $selection in
		1) clear; update_fedora ; press_enter ;;
		2) clear; xorg-server ; press_enter ;;
		3) clear; utilities; press_enter;;
		4) clear; tui-cli-apps; press_enter;;
		5) clear; gui-apps; press_enter;;
		6) clear; suckless-apps;;
		7) clear; directories-configs; press_enter;;
		8) clear; display-manager; press_enter;;
		0) clear; exit ;;
		*) clear; incorrect_selection ; press_enter ;;
	esac
done
